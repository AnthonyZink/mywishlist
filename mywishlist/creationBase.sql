-- phpMyAdmin SQL Dump
-- version 4.1.14.8
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 17 Janvier 2018 à 19:40
-- Version du serveur :  5.1.73
-- Version de PHP :  7.0.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `saltutti1u`
--

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE IF NOT EXISTS `commentaire` (
  `idCom` int(11) NOT NULL AUTO_INCREMENT,
  `content` text,
  `id_item` int(11) DEFAULT NULL,
  `nomUser` varchar(255) DEFAULT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`idCom`),
  KEY `id_item` (`id_item`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Contenu de la table `commentaire`
--

INSERT INTO `commentaire` (`idCom`, `content`, `id_item`, `nomUser`, `updated_at`, `created_at`) VALUES
(18, 'Bon courage ._.', 4, 'Mamie', '2018-01-17', '2018-01-17'),
(19, 'J&#39;espère que dans celle-la tu n&#39;auras pas d&#39;ampoule', 8, 'Parrain', '2018-01-17', '2018-01-17'),
(20, 'Ouah j&#39;adore', 13, 'ZINK Anthony', '2018-01-17', '2018-01-17');

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `id_item` int(11) NOT NULL,
  `img` varchar(300) NOT NULL,
  `url` int(11) DEFAULT '0',
  PRIMARY KEY (`id_item`,`img`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `image`
--

INSERT INTO `image` (`id_item`, `img`, `url`) VALUES
(1, 'https://psmedia.playstation.com/is/image/psmedia/ps4-overview-screen-03-eu-06sep16?$MediaCarousel_LargeImage$', 1),
(2, 'toto-1-slip-elephant_imagesia-com_u8yk_large.jpg', 0),
(3, 'http://www.golfpratique.com/wp-content/uploads/2014/04/club-fers.jpg', 1),
(4, 'toto-Scalpelle-1-scalp.jpg', 0),
(5, 'https://www.academiedugout.fr/images/5513/1200-auto/saucisse-toulouse-copy.jpg?poix=50&poiy=50', 1),
(5, 'toto-Saucisse-2-grosse-saucisse-nature.jpg', 0),
(6, 'toto-Kipa-1-kipa.jpg', 0),
(6, 'toto-Kipa-2-téléchargement.jpg', 0),
(7, 'http://i.ebayimg.com/00/s/MTAyNFgxMDI0/z/PpUAAOSw44BYQr-9/$_12.JPG?set_id=880000500F', 1),
(8, 'Jena-7-L39195900.jpg', 0),
(9, 'https://www.francetvinfo.fr/image/759r5hfru-3e8f/908/510/10507493.jpg', 1),
(10, 'tata-Poney electrique-1-poney.jpg', 0),
(11, 'https://www.ptitchef.com/imgupl/recipe/gateau-princesse--md-267609p434739.jpg', 1),
(12, 'tata-Kit-1-kit.jpg', 0),
(13, 'http://www.lockhatters.co.uk/media/catalog/product/cache/1/main-carousel-image/495x495/0dc2d03fe217f8c83829496872af24a0/f/e/fez_red_2.jpg', 1);

-- --------------------------------------------------------

--
-- Structure de la table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `liste_id` int(11) NOT NULL,
  `nom` text NOT NULL,
  `descr` text,
  `url` text,
  `tarif` decimal(5,2) DEFAULT NULL,
  `reserve` int(11) NOT NULL,
  `cagnotte` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Contenu de la table `item`
--

INSERT INTO `item` (`id`, `liste_id`, `nom`, `descr`, `url`, `tarif`, `reserve`, `cagnotte`) VALUES
(1, 43, 'Playstation 4', 'Je veux jouer avec mes amis !!', 'https://www.bonacheter.com/fr_FR/product/sony-playstation-4-ps4-slim-500go-avec-1-pi-ce-manette-sans-fil-noir-cuh-2106a-b01-a-region-blu-ray.html?matchtype=&adposition=1o1&gclid=EAIaIQobChMIo_qJ5Mff2AIVlGwbCh1dAwTFEAkYASABEgIROfD_BwE', '256.99', 0, 0),
(2, 43, 'Slip éléphant', 'Pour avoir chaud partout partout hihihi', '', '20.00', 0, 0),
(3, 43, 'Clubs de golf', 'Ceci est ma passion !', '', '299.00', 0, 0),
(4, 44, 'Scalpelle', 'Aie aie aie ', '', '30.00', 1, 0),
(5, 44, 'Saucisse', 'Il faut nourrire toute la famille', '', '30.00', 0, 1),
(6, 44, 'Kipa', 'Je veux être chic !', 'https://fr.aliexpress.com/item/JEWISH-KIPPAH-LEATHER-YARMULKE-KIPPAH-MINIMUM-ORDER-100PIECES/844605298.html?src=google&albslr=202404789&isdl=y&aff_short_key=UneMJZVf&source=%7Bifdyn:dyn%7D%7Bifpla:pla%7D%7Bifdbm:DBM&albch=DID%7D&src=google&albch=shopping&acnt=494-037-6276&isdl=y&albcp=653151748&albag=36672819047&slnk=&trgt=67337248418&plac=&crea=fr844605298&netw=g&device=c&mtctp=&gclid=EAIaIQobChMI54f6_Mjf2AIVhTLTCh2nNgT3EAQYAyABEgI4U_D_BwE', '10.00', 0, 0),
(7, 44, 'Train', 'J&#39;aime voyager.', '', '160.00', 0, 0),
(8, 47, 'Chaussure', 'Chaussure', '', '26.00', 1, 0),
(9, 48, 'Poney', 'C&#39;est mon plus grand rêve', '', '426.00', 0, 1),
(10, 48, 'Poney electrique', 'Si vous avez pas asssez de sous pour l&#39;autre, prenez celui-là. C&#39;est plus facile pour l&#39;entretien !', '', '40.00', 0, 0),
(11, 48, 'Gateau', 'Pour un anniv&#39; réussi, un gâteau serai le bienvenu !', '', '56.00', 0, 0),
(12, 48, 'Kit', 'Pour devenir une vraie princesse', '', '13.50', 0, 0),
(13, 49, 'Je suis reservé', 'reservé', '', '456.00', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `liste`
--

CREATE TABLE IF NOT EXISTS `liste` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `expiration` date DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estPublique` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=50 ;

--
-- Contenu de la table `liste`
--

INSERT INTO `liste` (`no`, `user_id`, `titre`, `description`, `expiration`, `token`, `estPublique`) VALUES
(43, 42, 'Liste de Nowel', 'Des cadeauuuuuux !', '2018-12-25', 'dMbEW8xTO1fCUY9An7bMAbK3GVWhYGZ.', 'false'),
(44, 42, 'Bar Mitsva', 'Je vais devenir un homme !', '2018-01-31', 'gQxEs9YiOjM/m.2W970y3IvYYs.41OPA', 'true'),
(45, 44, 'jean', 'La liste', '0000-00-00', 'yK8rR3lZe9JTBcuWvTrg7QTolEmM0WtD', 'false'),
(46, 44, 'jean', 'La liste', '0000-00-00', 'RW1jrne1e1XFbybxbFYaYQfnlK36dAeG', 'true'),
(47, 44, 'jean', 'La liste', '2019-02-06', '/8IBX5CSu.CJBjIpfIAmHvgeIML3DU0Q', 'true'),
(48, 45, 'Anniversaire', 'Pour un anniv&#39; de fée', '2018-04-13', '1exRZISnOvUzaIBTa2UAxWu1cy7DZ61A', 'true'),
(49, 45, 'Liste expiré', 'expiré', '2018-01-16', 'a1Uz7SjauqwgaITKsu4tF5Ca9etQEsky', 'false');

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `roleid` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `auth_level` int(11) NOT NULL,
  PRIMARY KEY (`roleid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `role`
--

INSERT INTO `role` (`roleid`, `label`, `auth_level`) VALUES
(1, 'admin', 5000);

-- --------------------------------------------------------

--
-- Structure de la table `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `user_uid` int(11) NOT NULL,
  `role_roleid` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `role_user`
--

INSERT INTO `role_user` (`user_uid`, `role_roleid`) VALUES
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(256) NOT NULL,
  `email` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`uid`, `username`, `password`, `email`, `nom`, `prenom`) VALUES
(42, 'toto', '$2y$12$G1uveNyfvHLy5uU6WrM99.FUXv.9oNh4lieqxZACwH6ExEcfrtpKS', 'toto@toto.fr', 'toto', 'toto'),
(45, 'tata', '$2y$12$VJ0d.Ec8.xPLBYmlScJZFOFvU1hjtsm3nEOdnfCzGGFx7Gzj1Ousu', 'tata@tata.fr', 'tata', 'tata'),
(44, 'Jena', '$2y$12$SkvOvuWDD1lJNf7Zrfe65.GZB5/z3llvkHgiFyOuEWATBJi4YbzsS', 'Jena@Jena.Jena', 'Jena', 'Jena');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD CONSTRAINT `commentaire_ibfk_1` FOREIGN KEY (`id_item`) REFERENCES `item` (`id`);

--
-- Contraintes pour la table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `image_ibfk_1` FOREIGN KEY (`id_item`) REFERENCES `item` (`id`);


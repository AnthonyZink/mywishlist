<?php
session_start();
require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;


$db = new DB();

$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();



$app = new \Slim\Slim();


//accueil




$app->get('/',function (){
    $app = \Slim\Slim::getInstance();
    $app->redirect('home');
});

$app->get('/home',function (){
    $c = new \mywishlist\controllers\CreateurController;
    $c->getAccueil();
})->name('home');

//contact

$app->get('/contact',function (){
    $c = new \mywishlist\controllers\CreateurController();
    $c->getContact();
})->name('contact');

//liste

$app->get('/liste',function (){
    $c = new \mywishlist\controllers\VisiteurControlleur();

    //si un utilisateur est connecte
    if(isset($_SESSION['profile'])){
        $c->getListe();
    }else{
        $c->getListe("externe");

    }

})->name('liste');



$app->get('/modifyListe', function() use ($app){
    if(isset($_SESSION['profile'])){
        $c = new \mywishlist\controllers\CreateurController();
        $c->getModifyListe();
    }else{
        $app->redirect('connexion');
    }
})->name('modifyListe');

$app->post('/modifyListe', function(){
    $c = new \mywishlist\controllers\CreateurController();
    $c->postModifyListe();
});

//liste_publique

$app->get('/liste_publique',function (){
    $c = new \mywishlist\controllers\VisiteurControlleur();
    $c->getListePublique();
})->name('liste_publique');

$app->post('/liste_publique',function (){
    $c = new \mywishlist\controllers\VisiteurControlleur();
    $c->postListePublique();
});

//item
$app->get('/item',function (){
    $c = new \mywishlist\controllers\VisiteurControlleur();
    $c->getItem();
})->name('item');


//connexion

$app->get('/connexion',function () use ($app){
    if(isset($_SESSION['profile'])){
        $app->redirect('dashboard');
    }
    $c = new \mywishlist\controllers\CreateurController();
    $c->getConnexion();
})->name('connexion');



$app->post('/connexion',function (){
    $c = new \mywishlist\controllers\CreateurController();
    $c->postConnexion();
});



//inscription


$app->get('/inscription',function () use ($app){
    if(isset($_SESSION['profile'])){
        $app->redirect('dashboard');
    }
    $c = new \mywishlist\controllers\CreateurController();
    $c->getInscription();
})->name('inscription');

$app->post('/inscription',function (){
    $c = new \mywishlist\controllers\CreateurController();
    $c->postInscription();
});


//deconnexion

$app->get('/deconnexion',function (){
    $c = new \mywishlist\controllers\CreateurController();
    $c->getDeconnexion();
})->name('deconnexion');




//profil

$app->get('/profil', function () use ($app){
    if(isset($_SESSION['profile'])){
        $c = new \mywishlist\controllers\CreateurController();
        $c->getProfil();
    }else{
        $app->redirect('connexion');
    }

})->name('profil');

$app->post('/profil', function (){
    $c = new \mywishlist\controllers\CreateurController();
    $c->postProfil();
});



//tableau de bord
$app->get('/dashboard' , function(){

    if(isset($_SESSION['profile'])){
        $c = new \mywishlist\controllers\CreateurController();
        $c->getDashboard();
    }else{
        $c = new \mywishlist\controllers\CreateurController();
        $c->getConnexion();
    }
})->name('dashboard');


//supprimer une liste
$app->get('/dropList', function() use ($app){
   if(isset($_SESSION['profile']) && isset($_GET['token'])){

        $lists = \mywishlist\models\Liste::where('user_id',"=",$_SESSION['profile']['userid'])->get();
        $owner = false;

        foreach ($lists as $list){
            if($list->token === $_GET['token']){
                $owner = true;
            }
        }

        if($owner == true){
            $c = new \mywishlist\controllers\CreateurController();
            $c->dropList();
        }else{
            $app->redirect('home');
        }
    }else{

        $app->redirect('home');
    }
})->name('dropList');

//creer une liste
$app->get('/createList', function(){
    $c = new \mywishlist\controllers\CreateurController();
    $c->getCreationListe();
})->name('createList');

$app->post('/createList',function (){
    $c = new \mywishlist\controllers\CreateurController();
    $c->postCreateList();
});

//creer un item
$app->get('/createItem',function () use ($app){
    if(isset($_SESSION['profile'])){
        $c = new \mywishlist\controllers\CreateurController();
        $c->getCreateItem();
    }else{
        $app->redirect('connexion');
    }

})->name('createItem');


$app->post('/createItem',function (){
    $c = new \mywishlist\controllers\CreateurController();
    $c->postCreateItem();
});

$app->get('/modifyItem', function() use ($app){
    if(isset($_SESSION['profile'])){
        $c = new \mywishlist\controllers\CreateurController();
        $c->getModifyItem();
    }else{
       $app->redirect('connexion');
    }
})->name('modifyItem');

$app->post('/modifyItem', function(){
    $c = new \mywishlist\controllers\CreateurController();
    $c->postModifyItem();
});

//suppression article

$app->get('/deleteItem',function () use ($app){
    if(isset($_SESSION['profile'])){
        $c = new \mywishlist\controllers\CreateurController();
        $c->deleteItem();
    }else{
       $app->redirect('connexion');
    }
})->name('deleteItem');


//partager une liste
$app->get('/partagerListe', function() use ($app){
    if(isset($_SESSION['profile'])){
        $c = new \mywishlist\controllers\VisiteurControlleur();
        $c->getPartagerListe();
    }else{
       $app->redirect('connexion');
    }

})->name('partagerListe');

$app->post('/partagerListe', function (){
    $c = new \mywishlist\controllers\VisiteurControlleur();
    $c->postPartagerListe();
});




//reserver une liste
$app->get('/reserver',function(){
    $c = new \mywishlist\controllers\VisiteurControlleur();
    $c->getReserverItem();
})->name('reserver');

$app->post('/reserver',function (){
    $c = new \mywishlist\controllers\VisiteurControlleur();
    $c->postReserverItem();
});


$app->get('/dropImage', function(){
    $c = new \mywishlist\controllers\CreateurController();
    $c->dropImage();
})->name('dropImage');


//commentaires
$app->get('/commentaire',function (){

    $c = new \mywishlist\controllers\CreateurController();

    if(isset($_GET['id']))
        $c->getCommentaire();
    else
        $c->getAccueil();
})->name('commentaire');

//liste des créateurs
$app->get('/createurs',function (){
    $c = new \mywishlist\controllers\VisiteurControlleur();
    $c->getListCreateurs();
})->name('createurs');


$app->run();
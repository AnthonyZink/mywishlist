<?php

namespace mywishlist\controllers;


use mywishlist\views\ListePubliqueView;
use mywishlist\views\ItemView;
use mywishlist\views\ListeView;
use mywishlist\views\PartagerListeView;
use mywishlist\views\ReservationView;
use mywishlist\views\CreateurListView;
use Slim\Slim;

class VisiteurControlleur
{

    //vue d'un item
    public function getItem(){
	$v = new ItemView();
	$v->render();
    }


    //vue d'une liste publique
    public function getListePublique(){
        $trie="expiration";
        $v = new ListePubliqueView($trie);
        $v->render();
    }

    public function postListePublique(){
        if(isset($_POST['trier'])){

            $trie = $_POST['trier'];
        }
        $v = new ListePubliqueView($trie);
        $v->render();

    }

    public function getListe($etatUser = "interne"){
        if(isset($_GET['token'])){
            $v = new ListeView($etatUser);
            $v->render();
        }else{
            $app = \Slim\Slim::getInstance();
            $app->redirect('home');
        }

    }

    //listes des createurs
    public function getListCreateurs(){
        $v = new CreateurListView();
        $v->render();
    }


    //partage d'une liste
    public function getPartagerListe(){
        $v = new PartagerListeView();
        $v->render();
    }

    public function postPartagerListe(){
        $v = new PartagerListeView();
        $v->render();

    }


    public function getReserverItem(){

        $v = new ReservationView($_GET['token'],$_GET['id']);
        $v->render();
    }

    public function postReserverItem(){
        include 'actions/reserver.php';
        $app = Slim::getInstance();
        $app->redirect('home');
    }

}
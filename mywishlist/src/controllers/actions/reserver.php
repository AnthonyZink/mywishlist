<?php


$app = \Slim\Slim::getInstance();

//on verifie que le formulaire soit rempli completement
if(isset($_POST['textRes'],$_POST['nom'],$_POST['id']) and !empty($_POST['id']) and !empty($_POST['textRes']) and !empty($_POST['nom'])){

    $id = filter_var($_POST['id'], FILTER_VALIDATE_INT);
    $item = \mywishlist\models\Item::where('id', '=', $id)->first();

    if (!is_null($item)) {

        $nom = filter_var($_POST['nom'], FILTER_SANITIZE_STRING);
        $text = filter_var($_POST['textRes'], FILTER_SANITIZE_STRING);

        //si il n'y a pas de cagnotte
        if(!isset($_POST['cagnotte'])) {

            $c = new \mywishlist\models\Commentaire();
                $c->content = $text;
                $c->id_item = $id;
                $c->nomUser = $nom;
                $c->save();

                $item->reserve = 1;
                $item->save();

                $_SESSION['userReserv'] = $nom;


        //

            //si il y a une cagnotte
        }else{
            $montantReserver = filter_var($_POST['cagnotte'], FILTER_VALIDATE_FLOAT);

            //si le montant n'est pas plus grand que la cagnotte et positif
            if($montantReserver > 0 && $montantReserver <= $item->tarif){
                $newMontant = $item->tarif - $montantReserver;

                if($newMontant == 0){
                    $item->reserve = 1;
                }

                $c = new \mywishlist\models\Commentaire();
                $c->content = $text;
                $c->id_item = $id;
                $c->nomUser = $nom;
                $c->save();

                $item->tarif = $newMontant;
                $item->save();

                $_SESSION['userReserv'] = $nom;
            }else{
                $app->flash('error', 'Montant incorrect');
                $app->redirect("modifyListe?token=$token");
            }
        }
    }else {
        $app->flash('error', "L'item n'existe pas");
        $app->redirect("modifyListe?token=$token");
    }

}else{
    $app->flash('error', 'Veuillez remplir tous les champs');
    $app->redirect("modifyListe?token=$token");
}





<?php

use mywishlist\models\Liste;

//verification que le token soit passe en parametre de l'url
if(isset($_GET['token'])){

    //recuperation et suppression de la liste
    $listToDestroy = Liste::where('token', '=', $_GET['token'])->first();

    $items = $listToDestroy->items()->get();

    foreach($items as $item){

        $imgs = $item->images()->get();
        foreach($imgs as $img){
            $img->delete();
        }
        $comms = $item->commentaire()->get();
        foreach($comms as $comm){
            $comm->delete();
        }

        $item->delete();
    }

    $listToDestroy->delete();


}else{
    $app = \Slim\Slim::getInstance();
    $app->redirect("home");
}






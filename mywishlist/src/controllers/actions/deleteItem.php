<?php
$app = \Slim\Slim::getInstance();

//on verifie si l'id et le le token sont en parametres
if(isset($_GET['id']) && isset($_GET['token'])){

    //filtration des donnees
    $id = filter_var($_GET['id'],FILTER_VALIDATE_INT);
    $token = filter_var($_GET['token'],FILTER_SANITIZE_STRING);

    //s'il existe un item ayant l'id en parametre
    if($u = \mywishlist\models\Item::where('id','=',$id)->first()){

        //on regarde si il a une liste associe
        if($liste = $u->liste){

            //on regarde si la liste de l'item a le bon token passe en parametre de l'url
            if($liste->token === $_GET['token']){

                //si il y a une image

                foreach($u->images()->get() as $img){
                    //suppression de l'image ---- dirname(dirname(dirname(__DIR__)))."/

                    if($img->url === 1){
                        $img->delete();
                    }else{

                        unlink("assets/img/" . $img->img);
                    }
                }

                //suppression de l'item
                 $u->delete();
            }else{
                $app->flash('error', 'Vous ne pouvez pas supprimer cet item');
                $app->redirect("liste?token=$token");
            }

        }else{
            $app->redirect("dashboard");
        }
    }else{
        $app->redirect("dashboard");
    }
}else{
    $app->redirect("dashboard");
}




cd <?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 30/12/2017
 * Time: 15:02
 */

use \mywishlist\models\Item;
$app = \Slim\Slim::getInstance();

$token = filter_var($_GET['token'],FILTER_SANITIZE_STRING);
$idItem = filter_var($_GET['id'], FILTER_VALIDATE_INT);

$item = Item::find($idItem);

$tokenListe = $item->liste()->first()->token;


if($token == $tokenListe){

    if(isset($_POST['addImage'])) {


        if(is_uploaded_file($_FILES['file']['tmp_name'])) {

            $max = \mywishlist\models\Image::where(['id_item' => $idItem])->count() + 1;

            $key = $_SESSION['profile']['username'] . "-" . $item->nom . "-$max";
            $extensions_valides = array('jpg', 'jpeg', 'png');
            $dir = dirname(dirname(dirname(__DIR__))) . "/assets/img";

            $extension_upload = strtolower(substr(strrchr($_FILES['file']['name'], '.'), 1));

            if (in_array($extension_upload, $extensions_valides)) {

                $name = $key . "-" . $_FILES["file"]["name"];

                $res = move_uploaded_file($_FILES['file']['tmp_name'], "$dir/$name");

                $image = new \mywishlist\models\Image();
                $image->id_item = $idItem;
                $image->img = $name;
                $image->save();

            }
            $app->flash('error', "L'extension du fichier n'est pas acceptée !");
            $app->redirect('modifyItem?id='.$idItem .'&token=' . $token);
        }else {
            $app->redirect('modifyItem?id='.$idItem .'&token=' . $token);
        }
    }else{
        if(empty($_POST['itemName']) || empty($_POST['description']) || empty($_POST['price'])){
            $app->flash('error', "Veuillez remplir toutes les informations !");
            $app->redirect('modifyItem?id='.$idItem . '&token='.$token);
        }else{
            $nom = filter_var($_POST['itemName'],FILTER_SANITIZE_STRING);
            $description = filter_var($_POST['description'],FILTER_SANITIZE_STRING);
            $prix = filter_var($_POST['price'],FILTER_VALIDATE_FLOAT);

            $link ="";
            if(isset($_POST['link'])){
                $link = filter_var($_POST['link'],FILTER_SANITIZE_STRING);
            }

            $urlImage ='';
<<<<<<< HEAD
            if(isset($_POST['urlImage']) && $_POST['urlImage']!=""){
=======
            if(isset($_POST['urlImage']) && $_POST['urlImage'] != ''){
>>>>>>> f953bdefcf6be52b997dae8c2475b90a9f2d506b
                $urlImages = filter_var($_POST['urlImage'],FILTER_SANITIZE_URL);
                $urlImage = filter_var($urlImages,FILTER_VALIDATE_URL);

                $img = $item->images()->where('url','=',1)->first();


                if($urlImage || empty($urlImages)){
                    if(empty($urlImages) && isset($img)){
                        $img->delete();
                    }
                    elseif($img){

                        $img->img = $urlImage;
                        $img->url = 1;
                        $img->save();
                    }else{
                        $img = new \mywishlist\models\Image();
                        $img->id_item = $idItem;
                        $img->img = $urlImage;
                        $img->url = 1;
                        $img->save();
                    }
                }else{
                    $app->flash('error', "L'url est incorrecte");
                    $app->redirect('modifyItem?id='.$idItem . '&token='.$token);
                }

            }else{

            }

            $item->nom = $nom;
            $item->descr = $description;
            $item->tarif = $prix;
            $item->url = $link;

            if($item->tarif !== 0){
                $item->reserve = 0;
            }

            $item->save();
            $app->redirect('liste?token='.$token);
        }

    }
}
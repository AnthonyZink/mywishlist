<?php

$app = \Slim\Slim::getInstance();

//on verifie qu'un formulaire a ete poste
if (isset($_POST)){

    //cas où le formulaire est pour le changement de mot de passe
    if(isset($_POST['password'])){
        $password = password_hash(filter_var($_POST['password'],FILTER_SANITIZE_STRING), PASSWORD_DEFAULT,['cost' => 12]);
        \mywishlist\models\User::where('username',"=",$_SESSION['profile']['username'])->update(['password' => $password]);

    }

    //cas où le formulaire est pour le changement du nom de l'utilisateur
    if(isset($_POST['nom'])){
        $nom = filter_var($_POST['nom'],FILTER_SANITIZE_STRING);
        \mywishlist\models\User::where('username',"=",$_SESSION['profile']['username'])->update(['nom' => $nom]);

    }

    //cas où le formulaire est pour le changement du prenom de l'utilisateur
    if(isset($_POST['prenom'])){
        $prenom =filter_var($_POST['prenom'],FILTER_SANITIZE_STRING);
        \mywishlist\models\User::where('username',"=",$_SESSION['profile']['username'])->update(['prenom' => $prenom]);

    }

    //cas où le formulaire est pour le changement de l'adresse email de l'utilisateur
    if(isset($_POST['email'])){
        $email = filter_var($_POST['email'],FILTER_VALIDATE_EMAIL);
        if($email){

            //verification que la nouvelle adresse email ne soit pas deja utilise
            $tmp = \mywishlist\models\User::where('email','=',$email);
            if($tmp==null){

                \mywishlist\models\User::where('username',"=",$_SESSION['profile']['username'])->update(['email' => $email]);
            }else{
                echo "vous en pouvez pas mettre cette adresse email";
                die();
            }
        }else{
            $app->flash('error', 'Adresse email non valide');
            $app->redirect('profil');
        }
    }

    //cas où le formulaire est pour la suppression d'un compte (suppression compte + role de l'utilisateur)
    if(isset($_POST['delete'])){
        $user = \mywishlist\models\User::where('username','=',$_SESSION['profile']['username'])->first();


        $role_user = \mywishlist\models\Role_user::where('user_uid','=',$user->uid)->first();
        $lists = $user->lists;

        foreach ($lists as $list){

            foreach ($list->items as $item){
                foreach ($item->images as $img){
                    $i = dirname(dirname(dirname(__DIR__)))."/assets/img/$img->img";
                    unset($i);
                    $img->delete();
                }

                if(($it = \mywishlist\models\Commentaire::where('id_item',$item->id)->first()) !== null){
                    $it->delete();

                }


                $item->delete();
            }
            $list->delete();
        }

        $user->delete();

    }



}
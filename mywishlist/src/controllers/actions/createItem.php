<?php

use \mywishlist\models\Item;
$app = \Slim\Slim::getInstance();

//on verifie la presence du token
if(isset($_GET['token'])) {
    $token = filter_var($_GET['token'],FILTER_SANITIZE_STRING);


    //cas où le formulaire est pour l'ajout d'une image
    if (isset($_POST['addImage'])) {

        $max = Item::max('id');

        //tableau des extensions acceptes
        $extensions_valides = array('jpg', 'jpeg', 'png');

        //dossier contenant les images des items
        $dir = dirname(dirname(dirname(__DIR__))) . "/assets/img";

        //extension du fichier
        $extension_upload = strtolower(substr(strrchr($_FILES['file']['name'], '.'), 1));

        //on verifie que l'extension est presente dans le tableau
        if (in_array($extension_upload, $extensions_valides)) {

            //change le nom du fichier
            $name = $_SESSION['profile']['username'] . "-" . $max . "-" . $_FILES["file"]["name"];

            //deplace le fichier dans notre dossier img
            $res = move_uploaded_file($_FILES['file']['tmp_name'], "$dir/$name");

            //ajoute l'image en session pour pouvoir l'afficher
            $_SESSION['image'][] = $name;

            $app->redirect('createItem?token=' . $token);
        } else {
            $app->flash('error', "L'extension du fichier n'est pas accepté !");
            $app->redirect('createItem?token=' . $token);
        }
    //cas où le formulaire est pour l'ajout de l'item
    } else {
        //recupere la liste de l'item
        $list = \mywishlist\models\Liste::where("token", "=", $token)->first();

        //on verifie  que tous les champs soient remplis
        if (empty($_POST['itemName']) || empty($_POST['description']) || empty($_POST['price'])) {
            $app->flash('error', "Veuillez remplir toutes les informations !");
            $app->redirect('createItem?token=' . $token);
        }



        //filtration des donnees
        $nom = filter_var($_POST['itemName'], FILTER_SANITIZE_STRING);
        $description = filter_var($_POST['description'], FILTER_SANITIZE_STRING);
        $prix = filter_var($_POST['price'], FILTER_VALIDATE_FLOAT);

        //verification si le lien de l'objet est present
        $link ="";
        if(isset($_POST['link'])){
            $link = filter_var($_POST['link'],FILTER_SANITIZE_STRING);
        }



        //on verifie que le prix est positif et plus grand que 0
        if($prix <= 0){
            $app->flash('error', "Le prix doit etre superieur à 0 !");
            $app->redirect('createItem?token=' . $token);
        }

        //on verifie si il y a une cagnotte
        $cagnotte = 0;

        if(isset($_POST['cagnotte'])){
            $cagnotte = 1;
        }

        //creation du nouvel item

        $idMax = \mywishlist\models\Item::max('id') + 1;

        $item = new Item();
        $item->id = $idMax;
        $item->liste_id = $list->no;
        $item->nom = $nom;
        $item->descr = $description;
        $item->tarif = $prix;
        $item->url = $link;
        $item->reserve = 0;
        $item->cagnotte = $cagnotte;


        $item->save();
        $urlImage = '';
        if(!empty($_POST['urlImage'])){
            $urlImages = filter_var($_POST['urlImage'],FILTER_SANITIZE_URL);
            $urlImage =  filter_var($urlImages,FILTER_VALIDATE_URL);

            if($urlImage || empty($urlImages)){

                $i = new \mywishlist\models\Image();
                $i->id_item = $idMax;
                $i->img = $urlImage;
                $i->url = 1;
                $i->save();
            }else{
                $app->flash('error', "L'url est incorrect");
                $app->redirect('createItem?token=' . $token);
            }
        }

        //ajout des images en fonction de la session
        if (isset($_SESSION['image'])) {
            foreach($_SESSION['image'] as $img){

                $image = new mywishlist\models\Image();
                $image->id_item = $idMax;
                $image->img = $img;
                $image->save();
            }
            unset($_SESSION['image']);
        }


        $app->redirect('liste?token=' . $token);


    }

}


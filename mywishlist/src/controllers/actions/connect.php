<?php

//verification que le formulaire de connexion soit complet
if(isset($_POST['username']) && isset($_POST['password']) && !empty($_POST['username']) && !empty($_POST['password'])){

    $username = filter_var($_POST['username'],FILTER_SANITIZE_STRING);

    //hachage du mot de passe
    $password = filter_var($_POST['password'],FILTER_SANITIZE_STRING);

    //authentification de l'utilisateur
    \mywishlist\models\Authentification::authentificate($username,$password);


}else{
    $app = \Slim\Slim::getInstance();
    $app->flash('error', 'Veuillez remplir tous les champs');
    $app->redirect('connexion');

}
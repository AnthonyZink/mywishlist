<?php
/**
 * Created by PhpStorm.
 * User: matthieu
 * Date: 23/12/2017
 * Time: 17:45
 */

//on verifie que les infomrations poster dans le formulaire sont valides
if(isset($_POST['username'], $_POST['description'],$_POST['date'],$_POST['estPublique'],$_SESSION['profile']['userid']) and !empty($_POST['username']) and !empty($_POST['description']) and !empty($_POST['date']) and !empty($_POST['estPublique'])){

    //filtration des donnees
    $titre = filter_var($_POST['username'],FILTER_SANITIZE_STRING);
    $description = filter_var($_POST['description'],FILTER_SANITIZE_STRING);
    $date = filter_var($_POST['date'],FILTER_SANITIZE_STRING);
    $publique = filter_var($_POST['estPublique'],FILTER_SANITIZE_STRING);

    //creation d'un token unique pour une liste
    $token = substr(password_hash(date("dmy-Gi"),PASSWORD_DEFAULT),20,32);

    //creation d'une liste

    $list = new \mywishlist\models\Liste();

    $list->user_id = $_SESSION['profile']['userid'];
    $list->titre = $titre;
    $list->description = $description;
    $list->expiration = $date;
    $list->token = $token;
    $list->estPublique = $publique;
    $list->save();

    if(isset($_COOKIE['list'])){
        $tab = unserialize($_COOKIE['list']);
        $tab[] = \mywishlist\models\Liste::where(['token' => $token])->first()->no;
        setCookie('list',serialize($tab),time()+60*60*24*365);
    }else{
        $tab[] = \mywishlist\models\Liste::where(['token' => $token])->first()->no;
        setCookie('list',serialize($tab),time()+60*60*24*365);
    }

}else{
    $app = \Slim\Slim::getInstance();
    $app->flash('error', 'Veuillez remplir tous les champs');
    $app->redirect('createList');
}



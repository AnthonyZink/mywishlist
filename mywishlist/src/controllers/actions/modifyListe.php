<?php
/**
 * Created by PhpStorm.
 * User: matthieu
 * Date: 23/12/2017
 * Time: 17:45
 */


$token = $_GET['token'];
$app = \Slim\Slim::getInstance();

if(isset($token)){
    $list = \mywishlist\models\Liste::where('token', '=', $_GET['token'])->first();

    if(isset($_POST['username'], $_POST['description'],$_POST['date'],$_POST['estPublique']) and !empty($_POST['username']) and !empty($_POST['description']) and !empty($_POST['date']) and !empty($_POST['estPublique'])){

        $titre = filter_var($_POST['username'],FILTER_SANITIZE_STRING);
        $description = filter_var($_POST['description'],FILTER_SANITIZE_STRING);
        $date = filter_var($_POST['date'],FILTER_SANITIZE_STRING);
        $publique = filter_var($_POST['estPublique'],FILTER_SANITIZE_STRING);

        $list->titre = $titre;
        $list->description = $description;
        $list->expiration = $date;
        $list->estPublique = $publique;

        $list->save();

    }else{
        $app->flash('error', 'Veuillez remplir tous les champs');
        $app->redirect("modifyListe?token=$token");
    }
}else{
    $app->redirect('home');
}




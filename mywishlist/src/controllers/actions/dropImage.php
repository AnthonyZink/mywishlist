<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 03/01/2018
 * Time: 17:58
 */

use \mywishlist\models\Image;
use \mywishlist\models\Item;


if(isset($_GET['name'])){

    $name = filter_var($_GET['name'],FILTER_SANITIZE_STRING);

    unset($_SESSION['image'][array_search($name, $_SESSION['image'])]);
    unlink('assets/img/' . $name);
    $app = \Slim\Slim::getInstance();
    $app->redirect('createItem?token=' . $_GET['token']);
}else{
    $id = filter_var($_GET['id'],FILTER_VALIDATE_INT);
    $token = filter_var($_GET['token'],FILTER_SANITIZE_STRING);
    $img = filter_var($_GET['img'],FILTER_SANITIZE_STRING);

    $image = Image::where(["id_item" => $id, 'img' => $img])->first();
    $tokenItem = Item::where(['id' => $image->id_item])->first()->liste()->first()->token;

    if($tokenItem == $token){
        unlink("assets/img/" . $_GET['img']);
        $image->delete();
    }else{
        $app = \Slim\Slim::getInstance();
        $app->redirect('home');
    }

}







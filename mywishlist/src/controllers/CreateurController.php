<?php

namespace mywishlist\controllers;

use mywishlist\views\AccueilView;
use mywishlist\views\CommentairesView;
use mywishlist\views\ConnexionView;
use mywishlist\views\ContactView;
use mywishlist\views\InscriptionView;
use mywishlist\views\ProfilView;
use mywishlist\views\DashboardView;
use mywishlist\views\CreationListeView;
use mywishlist\views\EditItemView;
use mywishlist\views\ModificationListeView;
use Slim\Slim;

class CreateurController
{

    //page d'accueil
    public function getAccueil(){
        $v = new AccueilView();
        $v->render();
    }

    
    
    //page de contact
    public function getContact(){
        $v = new ContactView();
        $v->render();
    }

    
    
    //page de connexion
    public function getConnexion(){
        $v = new ConnexionView();
        $v->render();
    }

    public function postConnexion(){
        include 'actions/connect.php';
        $app = \Slim\Slim::getInstance();
        $app->redirect('dashboard');
    }

    

    //page d'inscription
    public function getInscription(){
        $v = new InscriptionView();
        $v->render();
    }

    public function postInscription(){
        include 'actions/addUser.php';
        $app = \Slim\Slim::getInstance();
        $app->redirect('home');
    }

    
    
    //deconnexion
    public function getDeconnexion(){
        include 'actions/disconnect.php';
        $app = \Slim\Slim::getInstance();
        $app->redirect('home');
    }

    
    
    //page du profil
    public function getProfil(){
        $v = new ProfilView();
        $v->render();
    }

    public function postProfil(){
        include 'actions/changeProfil.php';
        $this->getDeconnexion();
    }
    
    
    
    //tableau de bord
    public function getDashboard(){
        $v = new DashboardView();
        $v->render();
    }

    
    
    //Supprimer une liste
    public function dropList(){

        include 'actions/dropList.php';
        $app = \Slim\Slim::getInstance();
        $link = $app->request->getUrl().$app->request->getRootUri()."/dashboard";
        $app->redirect($link);
    }

    
    
    //Creer une liste
    public function getCreationListe(){
        $v = new CreationListeView();
        $v->render();
    }

    public function postCreateList(){
        include 'actions/createList.php';
        $app = \Slim\Slim::getInstance();
        $app->redirect("liste?token=$token");
    }

    
    
    //Creer un item
    public function getCreateItem(){
        if(isset($_GET['token'])){
            $token = filter_var($_GET['token'],FILTER_SANITIZE_STRING);
            $v = new EditItemView($token);
            $v->render();
        }else{
            $app = \Slim\Slim::getInstance();
            $app->redirect('home');
        }

    }

    public function postCreateItem(){
        include 'actions/createItem.php';
    }

    
    
    //Suppression d'un item
    public function deleteItem(){
        include 'actions/deleteItem.php';
        $app = \Slim\Slim::getInstance();
        $app->redirect("liste?token=$token");
    }

    
    
    //Modifier un item
    public function getModifyItem(){
        $app = \Slim\Slim::getInstance();
        if(isset($_GET['id'],$_GET['token'])){
            $item = \mywishlist\models\Item::where('id', '=', $_GET['id'])->first();
            if($item != null) {
                $tokenListe = $item->liste()->first()->token;
                if ($tokenListe != null && $tokenListe == $_GET['token']) { //null
                    $v = new EditItemView($_GET['token'], $item);
                    $v->render();
                }else
                    $app->redirect("home");
            }else
                $app->redirect("home");
        }else
            $app->redirect("home");

    }


    public function postModifyItem(){
        include 'actions/modifyItem.php';
    }

    //Modifier une liste
    public function getModifyListe(){
        $app = \Slim\Slim::getInstance();
        if(isset($_GET['token'])){
            $list = \mywishlist\models\Liste::where('token', '=', $_GET['token'])->first();
            $v = new ModificationListeView($list);
            $v->render();
        }else
            $app->redirect("home");
    }

    public function postModifyListe(){
        include 'actions/modifyListe.php';
        $app = \Slim\Slim::getInstance();
        $app->redirect("liste?token=$token");
    }


    public function dropImage(){
        include 'actions/dropImage.php';
        $app = \Slim\Slim::getInstance();
        $app->redirect("modifyItem?id=$id&token=$token");
    }


    //commentaires
    public function getCommentaire(){
        $v = new CommentairesView();
        $v->render();
    }





}
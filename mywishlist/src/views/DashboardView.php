<?php

namespace mywishlist\views;

use mywishlist\models\Liste;
use Slim\Slim;

class DashboardView extends GlobalView
{

    //methode d'affichage
    public function render(){

        $head = parent::head();
        $header = parent::header();

        $dashboard = $this->dashboard();



        $html = <<<END
<html lang='fr'>
    <head>
        $head
        <link rel='stylesheet' href='assets/css/dashboard.css'>
    </head>
    <body>
    
        $header
    
        $dashboard
        
        
    </body>
</html>
END;

        echo $html;

}

    //affiche toutes les listes de l'utilisateur connecte
    private function dashboard(){
        //Recupere toutes les listes

    $lists = Liste::where('user_id','=',$_SESSION['profile']['userid'])->get();


    $html = <<<END
        <section id="dashboard">
            <div class="container">

             <h2>Tableau de bord</h2>

            <ul class="grid-3 has-gutter">
END;
;
    //Parcours des listes
    $i = 1;
    $app = Slim::getInstance();
    foreach($lists as $list){
        $html .= "
           
           <li>
        <div class='text'>
          <p class='number-list'>#$i</p>
          <p class='title-list'>$list->titre</p>
        </div>

        <div class='hover'>
          <div class='about'>
            <a href='". $app->urlFor('liste')  . "?token=" . $list->token . "'><i class='fa fa-search-plus fa-4x' aria-hidden='true'></i></a>
          </div><!--
            @whitespace
        --><div class='delete'>
            <a id='del' href='" . $app->urlFor('dropList') . "?token=" . $list->token . "'><i class='fa fa-trash fa-4x' aria-hidden='true'></i></a>
          </div>
        </div>
      </li> 
            
";
    $i++;
    }

    $html .= "
    <li id='plus'>
    <a href='" . $app->urlFor('createList')  . "'><i class=\"fa fa-plus-circle fa-5x\" aria-hidden=\"true\"></i></a>
</li>
</ul>
  </div>

</section>
";


    return $html;
}
}
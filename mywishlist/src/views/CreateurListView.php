<?php


namespace mywishlist\views;


use mywishlist\models\Liste;

class CreateurListView extends GlobalView
{
    //methode d'affichage
    public function render(){
        $head = parent::head();
        $header = parent::header();

        $crea = $this->listCreateur();


        $html = "
<html lang='fr'>
    <head>
        $head
        
        <link rel='stylesheet' href='assets/css/createur.css'>
    </head>
    <body>
    
        $header
    
    
       $crea
        
    
    
    </body>
</html>";

        echo $html;
    }

    private function listCreateur(){

        $res = "
               
            <section id='createur'>
                <div class='container'>
                    <h1>Liste des créateurs</h1>
                    <h3>(qui ont une liste publique)</h3>
            
            
            
            
      
        ";


        $lists = Liste::where('estPublique','true')->get();
        $res .= "<ul>";
        $arr =[];
        foreach ($lists as $list){

            if(!in_array($list->users->username,$arr)){

                $res.="<li>".$list->users->username."</li>";
                array_push($arr,$list->users->username);
            }
        }
        $res.="</ul>
             </div>
            </section>
            ";


        return $res;
    }


}


<?php

namespace mywishlist\views;

use Slim\Slim;

class AccueilView extends GlobalView
{
    //methode d'affichage
    public function render(){

        $head = parent::head();
        $header = parent::header();

        $main = $this->main();
        $presentation = $this->presentation();
        $endPresentation= $this->endPresentation();

        $html = "
<html lang='fr'>
    <head>
    
        $head
    </head>
    <body>
    
        $header
    
    
        $main
    
    
        $presentation
  
        
    
    

    </body>
</html>
        ";

        echo $html;
    }

    //premiere section
    private function main(){

        $app = Slim::getInstance();

        if (isset($_SESSION['profile'])) {
            $url = $app->urlFor('dashboard');
        }
        else {
            $url = $app->urlFor('inscription');
        }


        return " <section id='main'>
            <div class='main-content'>
              <h1>Fais ta liste de cadeaux !</h1>
              <h3>Avec <span>MyWishList</span></h3>
              <h4>S3B { Berard • Sommer • Saltutti • Zink }</h4>
              <a class='btn-main' href='".$url."'>Commencer</a>
              <a href='".$app->urlFor('liste_publique')."'>Listes publiques</a>
              <a href='".$app->urlFor('createurs')."'>Listes des créateurs</a>
            </div>
          </div>
        </section>";
    }


    //deuxieme section
    private function presentation(){
        return " 
        <section id='presentation'>
          <div class='container grid-12-small-1'>
              <div class='col-4'>
                <div class='img-container'>
                  <img class='' src='assets/images/offrir.png' alt=''>
                </div>
              </div>
    
              <div class='col-8'>
                <div class='text-container'>
                  <h3>Des cadeaux tout le temps !</h3>
                  <p>Souvent déçu de vos cadeaux de noël et d'anniversaire ?<br>
                     Votre grand-mère vous offre-t-elle tous les ans le même pull en laine qui va traîner dans l'armoire ? <br>

                     Avec MyWishList, plus de problème : créez votre liste de voeux et partagez-la avec vos amis et votre famille. <br>

                     Laissez libre cours à vos envies et commencez dès maintenant à créer votre liste de souhaits !</p>
                </div>
            </div>
    
          </div>
        </section>";

    }


    //troisieme section
    private function endPresentation(){
        return "
        <section id='end-presentation'>
          <div class='container grid-12-small-1'>
            <div class='col-8'>
              <div class='text-container'>
                <h3>Même accessible depuis un smartphone !</h3>
                <p>Accédez à votre liste de voeux depuis n'importe où et tenez vous au courant des réservations de vos cadeaux où que vous soyez. <br>
                    Suivez facilement et en quelques clics l'avancement de votre liste à partir de votre smartphone ou tablette grâce à l'interface simple et épurée de MyWishList</p>
              </div>
            </div>
    
            <div class='col-4'>
              <div class='img-container'>
                <img class='' src='assets/images/phone.png' alt=''>
              </div>
            </div>
    
          </div>
        </section>";
    }

}
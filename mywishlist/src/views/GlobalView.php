<?php

namespace mywishlist\views;


use Slim\Slim;

class GlobalView
{
    //renvoie les scripts css et le titre
    public function head(){
        return "
                  <meta charset='UTF-8'>
                  <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                  <title>MyWishList</title>
                  <link rel='stylesheet' href='assets/css/grillade.css'>
                  <link rel='stylesheet' href='assets/css/global.css'>
                  <link rel='stylesheet' href='assets/css/header.css'>
                  <link rel='stylesheet' href='assets/css/index.css'>
                  
                  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">
                ";
    }

    //renvoie le menu
    public function header(){

        $app = Slim::getInstance();

        $nav = "";

        if(isset($_SESSION['profile'])){
            $nav = "<div id='user'>
                        <div id='username'>
                          <p>".$_SESSION['profile']['username']." <i class=\"fa fa-chevron-down\" aria-hidden=\"true\"></i></i></p>
                        </div>
                
                
                          <div id='sous-menu'>
                            <a href='".$app->urlFor("profil")."'>Mon profil</a>
                            <a href='".$app->urlFor("dashboard")."'>Tableau de bord</a>
                            <a href='".$app->urlFor("deconnexion")."'>Se deconnecter</a>
                          </div>
                    </div>";
        }else{
            $nav = "<a class='menu-item btn_inscription' href='".$app->urlFor('inscription')."'>Inscription</a>
                    <a class='menu-item btn_connexion' href='".$app->urlfor('connexion')."'>Connexion</a>";
        }

        return "

        <header>
          <div id='logo'>
            <p><a href='".$app->urlFor('home')."'>MyWishList</a></p>
            <div class='header-img'></div>
          </div>
    
        
          <nav>
          
          
          $nav
    
    
    
          </nav>
          <!--  -->
        </header>";
    }


    //affiche les differentes erreurs
    public function error(){
        if(isset($_SESSION['slim.flash']['error'])){
            return "<div class='errors'><p class='p_error'>".$_SESSION['slim.flash']['error']."</p></div>";
        }

        return "";
    }
}
<?php

namespace mywishlist\views;


class EditItemView extends GlobalView
{

    private $item, $token;

    public function __construct($token , $item=null){
        $this->item = $item;
        $this->token = $token;
    }

    //methode d'affichage
    public function render(){
        $head = parent::head();
        $header = parent::header();

        $modificationItem = $this->modificationItem();

        $html = "
<html lang='fr'>
    <head>
        $head
        
        <link rel='stylesheet' href='assets/css/editItem.css'>
    </head>
    <body>
    
        $header
    
    
        $modificationItem
        
    
   
    </body>
</html>";

        echo $html;
    }

    //partie de droite : modifier informations d'un item
    private function modificationItem(){

        if($this->item == null){
            $titre = "Nouvel Item";
            $nom = '';
            $desc = '';
            $prix = '';
            $link = '';
            $btn = 'Créer';
            $urlImage = '';
        }else{
            $titre = $this->item->nom;
            $nom = $this->item->nom;
            $desc = $this->item->descr;
            $prix = $this->item->tarif;
            $link = $this->item->url;
            $urlImage = '';
            $btn = 'Modifier';
        }

        $html = "

<section id='creationItem'>
<div class='container'>
  <div class='box'>
          
    <h2>" . $titre . " <i class='fa fa-gift' aria-hidden='true'></i></h2>
       
    <div class=''>
    
        <div id='upload'>
            <h3>Upload</h3>
            <div class='image_item grid-4 has-gutter'>".
            $this->afficherImages()
            ."
            </div>
        
           <form action='' method='post' enctype='multipart/form-data'>
                <input type='file' name='file' id='file'>
                <input type='submit' name='addImage' value='Ajouter'>
            </form>
         
        </div>
        

           <div id='edit-item'>
          
               <form class='col-1' action='' method='post'>
            
                    ".$this->urlImages()."
                    
                    <h3>Informations</h3>
                  <div>
                    <input type='text' name='itemName' required='' id='itemName' value='". $nom . "'>
                    <label for='itemName'>Nom de l'item *</label>
                  </div>
            
                  <div>
                    <textarea required='' name='description' id='description'>" . $desc . "</textarea>
                    <label for='description'>Description de l'item *</label>
                  </div>
            
                  <div id='box-price'>
                    <input type='text' name='price' required='' id='price' value='" . $prix . "'>
                    <label for='price'>Prix *</label>
                  </div>
                  
                   <div id='box-link'>
                    <input type='text' name='link' id='link' value='" . $link . "' placeholder='http://www.amazon.fr/chaussure'>
                    <label for='link'>Lien de votre objet</label>
                  </div>
                  
                   <div id='box-cagnotte'>
                      Voulez vous mettre une cagnotte sur cet item ?
                       ".$this->checkbox($this->item)."Oui
                  </div>
                  
                
                    
                    
                    
                        <input id='sub' type='submit' value=$btn>
                </form> 
            </div>
     </div>
  </div>
</div>
</section>";
        return $html;
    }

    //partie de gauche : modifier image
    private function afficherImages(){

        $s = '';
        if($this->item != null){
            $images = $this->item->images()->get();
            foreach($images as $img){
                if($img->url === 0){

                    $s .= "
                            <div class='img-container'>
                                 <img src='assets/img/" . $img->img . "' alt=''>
                                 <a href='dropImage?id=". $_GET['id']."&token=". $_GET['token'] . "&img=". $img->img. "'><i class=\"fa fa-trash fa-3x\" aria-hidden=\"true\"></i></a>
                            </div>
                            ";
                }else{
                    $s .= "
                            <div class='img-container'>
                                 <img src='$img->img' alt=''>
                                 
                            </div>
                            ";
                }
            }

        }else {
            if(isset($_SESSION['image'])){
                $images = $_SESSION['image'];

                foreach ($images as $img) {
                    $s .= "
                    <div class='img-container'>
                         <img src='assets/img/" . $img . "' alt=''>
                         <a href='dropImage?name=" . $img . "&token=". $_GET['token']."'><i class=\"fa fa-trash fa-3x\" aria-hidden=\"true\"></i></a>
                    </div>
                    ";
                }
            }

        }

        return $s;
    }

    private function checkbox($item){
        if(isset($item)){
            if($item->cagnotte === 1){
                return "<input type='checkbox'  id='oui' name='cagnotte' value='oui' checked>";
            }
        }
        return "<input type='checkbox'  id='oui' name='cagnotte' value='oui' ";
    }

    private function urlImages(){

        $res = "";

        if($this->item !== null){
            $images=$this->item->images()->where('url','=',1)->first();

<<<<<<< HEAD
            $t="";
            if($images->url === 1){
                $t = $images->img;
            }
=======

>>>>>>> f953bdefcf6be52b997dae8c2475b90a9f2d506b

            if(isset($images)){
                $res .= "
                    <div class='urlImage'>
                        <input type='text' name='urlImage' id='urlImage' value='" . $t . "' placeholder='http://www.amazon.fr/chaussure'>
                        <label for='urlImage'>Lien d'une image</label>
                  </div>";
            }else{
                $res .="<div class='urlImage'>
                        <input type='text' name='urlImage' id='urlImage' placeholder='http://www.amazon.fr/chaussure'>
                        <label for='urlImage'>Lien d'une image</label>
                  </div>";
            }



        }else{

            $res .="<div class='urlImage'>
                        <input type='text' name='urlImage' id='urlImage' value='' placeholder='http://www.amazon.fr/chaussure'>
                        <label for='urlImage'>Lien d'une image</label>
                  </div>";

        }



        return $res;
    }

}
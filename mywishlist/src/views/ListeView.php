<?php

namespace mywishlist\views;

use mywishlist\models\Commentaire;
use mywishlist\models\Item;
use mywishlist\models\Liste;
use Slim\Slim;


class ListeView extends GlobalView
{


    private $userType = '';

    public function __construct($etatUser) //externe de base
    {
        $this->userType =$etatUser;
    }


    //methode d'affichage
    public function render(){

        $head = parent::head();
        $header = parent::header();
        $app = \Slim\Slim::getInstance();

        if(isset($_COOKIE['list'])){

            $tab = unserialize($_COOKIE['list']);
            $idCour = Liste::where('token', '=', $_GET['token'])->first()->no;

            foreach($tab as $idList){

                if($idList == $idCour){
                    $this->userType = 'admin';
                    break;
                }
            }
        }

        $queryList = Liste::where('token', '=', $_GET['token'])->first();
        if ($queryList == null){
            $app->redirect('dashboard');
        }
        $user = $queryList->users;
        if(isset($_SESSION['profile']) && $_SESSION['profile']['username'] != $user->username){
            $this->userType = 'externe';
        }

        $liste = $this->liste();




        $html = "
<html lang='fr'>
    <head>   
        $head
        <link rel='stylesheet' href='assets/css/liste.css'>
    </head>
    <body>
    
        $header
    
    
        $liste
        
    
     
    </body>
</html>
        ";

        echo $html;
    }

    //affiche une liste
    private function liste(){
        $app = Slim::getInstance();
        $queryList = Liste::where('token', '=', $_GET['token'])->first();
        if ($queryList == null){
            $app->redirect('dashboard');
        }
        $user = $queryList->users;
        if(isset($_SESSION['profile']) && $_SESSION['profile']['username'] != $user->username){
            $this->userType = 'externe';
        }



        $str = "<section id='list'>
                    <div class='content'>
                                               
                        ".
                          $this->getDescription($queryList, $_GET['token'], $user)
                        ." 
                        
                        <section class='articles'>

                        ";


        $queryItem = Item::where('liste_id', '=', $queryList->no)->get();
        foreach ($queryItem as $res) {


            $img = $res->images()->first();
            if($img){

                if($img->url === 0)
                    $imgPath = "assets/img/" . $img->img;
                elseif ($img->url === 1){
                    $imgPath =$img->img;
                }
            }else{

                $imgPath = 'assets/images/null.jpg';
            }

            $nb_commentaires = Commentaire::where('id_item','=',$res->id)->count();

            $com ="";

            if($nb_commentaires > 0 && $queryList->expiration < date('Y-m-d'))
                $com = " <span class='icon icon--comment'><i class='fa fa-commenting-o' aria-hidden='true'></i></span><a href='commentaire?id=$res->id'>1 commentaire</a>";


            $str .= "
                
                  <article class='card'>
    <div class='card__thumb'>
      <img src='$imgPath' alt=''>
    </div>


    <div class='card__body'>
      <div class='card__body__category'>". $this->isReserved($res)."</div>
      <h2 class='card__body__title'><a href='item?id=". $res->id."'>" . $res->nom . " ".$this->isCagnotte($res)."</a> </h2>
      <div class='card__body__subtitle'>" . $res->tarif . "€</div>
      <div class='card__body__description'>
       <p>". $res->descr . "</p>".
                $this->get_card__body__description__btn($res->id)
            ."
           
      </div>
    </div>

    <div class='card__footer'>
        $com
    </div>
  </article>

                
                
                
                 
            ";
        }
        if($this->userType != 'externe' && $queryList->expiration > date('Y-m-d')){
            $str .= "
                    <article class='card card-add'>
            
                        <div class='card__thumb'>
                           <img src='assets/img/gift.jpg' alt=''>
                        </div>
                    
                    
                        <div class='card__body'>
                          <h2 class='card__body__title'><a href='".$app->urlFor('createItem')."?token=".$_GET['token']."'><i class=\"fa fa-gift\" aria-hidden=\"true\"></i> Ajoute un item <i class=\"fa fa-gift\" aria-hidden=\"true\"></i></a></h2>
                          <div class='card__body__subtitle'>N'hésite pas !</div>
                          <div class='card__body__description'>
                           <p>Envie d'ajouter des cadeaux à la liste de souhait ? C'est parti !</p>
                           <div class='card__body__description__btn grid'>
                                    <a href='".$app->urlFor('createItem')."?token=".$_GET['token']."'><i class=\"fa fa-plus-circle fa-5x btn-plus\" aria-hidden=\"true\"></i></a>
                           
                           </div>
                           
                          </div>
                           
                          
                         
                        </div>
            
               
              </article>";
        }
        
        $str .="
            </section>          
            </div>
            </div>
            </div>
        </section>";
        return $str;
    }

    //indique si l'item est une cagnotte
    private function isCagnotte($item){
        if($item->cagnotte === 1){
            return " (cagnotte)";
        }
        return "";
    }

    private function getDescription($queryList,$token, $user){
        $res = '';
        $app = \Slim\Slim::getInstance();

        $expiration ="";
        $modifList = "";

        if($queryList->expiration > date('Y-m-d')){
            $expiration = date_format(date_create($queryList->expiration),'d/m/Y');
            $modifList = "<a href='" . $app->urlFor('modifyListe') . "?token=" . $token . "'>
                    <i class='fa fa-pencil' aria-hidden='true'></i>
                    </a>
                    <a href='" . $app->urlFor('partagerListe') . "?token=" . $token . "'>
                    <i class='fa fa-share-alt-square' aria-hidden='true'></i>
                    </a>";
        }else{
            $expiration ="expiré";
        }

        if($this->userType == 'externe'){
            $res .= "
            
            <div class='description'>

                <div class='image'>
                    <img src='assets/images/man.svg' alt=''>
                </div>
                    
                <p class='description__user'>".$user->prenom." ".$user->nom." </p>  
                <p class='description__title'>" . $queryList->titre ." </p>
                <p class='description__detail'>". $queryList->description . "</p>
                <p class='description__share'><b>État de la liste</b> : " .$this->isPublique($queryList) ."</p>
                <p class='description__date'><b>Date de fin</b> : " . $expiration . "</p>
               
            </div>
            
            ";
        }else{
            $res .= "
            
            <div class='description'>

                                    
                <p class='description__title'>" . $queryList->titre ."
                    $modifList
                </p>
         
                <p class='description__detail'>". $queryList->description . "</p>
                <p class='description__share'><b>État de la liste</b> : " .$this->isPublique($queryList) ."</p>
                <p class='description__date'><b>Date de fin</b> : " . $expiration. "</p>
                 " .
                $this->link()       ."         
            </div>
            
            ";
        }
        return $res;
    }

    private function link(){

            $app = \Slim\Slim::getInstance();
            return "<p><span style='font-weight: bold'>Lien à envoyer</span> : " . $app->request->getUrl(). $app->request->getRootUri() . "/liste?token=" . $_GET['token'] . "</p>";

    }


    private function get_card__body__description__btn($itemID)
    {

        $res = '';
        $item = Item::where("id","=",$itemID)->first();
        $list = $item->liste;

        if ($this->userType != "externe" && isset($_SESSION['profile']) && $_SESSION['profile']['username'] == $item->liste->users->username && $list->expiration > date('Y-m-d')) {
            $res .= "
            <div class='card__body__description__btn grid-3'>
            <a href='item?id=" . $itemID . "&token=".$_GET['token']."'><i class=\"fa fa-search-plus fa-3x\" aria-hidden=\"true\"></i></a>   
            <a href='modifyItem?id=" . $itemID . "&token=" . $_GET['token'] . "'><i class=\"fa fa-pencil fa-3x\" aria-hidden=\"true\"></i></a>
            <a class='delete-btn' href='deleteItem?id=" . $itemID . "&token=" . $_GET['token'] . "'><i class=\"fa fa-trash fa-3x\" aria-hidden=\"true\"></i></i></a>
            </div>
        ";
        }elseif($this->userType == "admin"){
            $res .= "
            <div class='card__body__description__btn grid-3'>
            <a href='item?id=" . $itemID . "&token=".$_GET['token']."'><i class=\"fa fa-search-plus fa-3x\" aria-hidden=\"true\"></i></a>   
            <a href='modifyItem?id=" . $itemID . "&token=" . $_GET['token'] . "'><i class=\"fa fa-pencil fa-3x\" aria-hidden=\"true\"></i></a>
            <a class='delete-btn' href='deleteItem?id=" . $itemID . "&token=" . $_GET['token'] . "'><i class=\"fa fa-trash fa-3x\" aria-hidden=\"true\"></i></i></a>
            </div>
        ";
        }

        elseif ($item->reserve === 1 || $list->expiration < date('Y-m-d')){
            $res .= "
            <div class='card__body__description__btn grid-2'>
            <a href='item?id=". $itemID."&token=".$_GET['token']."'><i class=\"fa fa-search-plus fa-3x\" aria-hidden=\"true\"></i></a>   
            </div>";
        }else{
            $res .= "
            <div class='card__body__description__btn grid-2'>
            <a href='item?id=". $itemID."&token=".$_GET['token']."'><i class=\"fa fa-search-plus fa-3x\" aria-hidden=\"true\"></i></a>   
            <a href='reserver?token=".$_GET['token']."&id=".$itemID."'><i class=\"fa fa-credit-card fa-3x\" aria-hidden=\"true\"></i></a>
            </div>";
        }

        return $res;
    }

    //verifie si l'item est reserve
    private function isReserved($item){
        if($item->reserve == 0){
            return "Disponible";
        }else{
            return "Réservé";
        }
    }

    //verifie si la liste est publique
    private function isPublique($list){
        if($list->estPublique == 'true' && $list->expiration > date('Y-m-d')){
            return "Publique";
        }else{
            return "Privée";
        }
    }



}
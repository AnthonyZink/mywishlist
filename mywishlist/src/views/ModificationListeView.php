<?php

namespace mywishlist\views;


class ModificationListeView extends GlobalView
{


    private $list;

    public function __construct($list){
        $this->list = $list;
    }
    public function render(){
        $head = parent::head();
        $header = parent::header();

        $creationListe = $this->modificationListe();

        $html = "
<html lang='fr'>
    <head>
        $head
        
        <link rel='stylesheet' href='assets/css/creationListe.css'>
    </head>
    <body>
    
        $header
    
    
        $creationListe
        
    </body>
</html>";

        echo $html;
    }


    private function modificationListe(){
        $html = "

<section id='creationListe'>
      <div class='container'>
            <div class='box'>
            ".parent::error()."
    <h2>Modification de liste <i class='fa fa-gift' aria-hidden='true'></i></h2>
    <form action='' method='post'>
      <div>
        <input type='text' name='username' required='' id='username' value='". $this->list->titre  . "'>
        <label for='username'>Nom de la liste *</label>
      </div>

      <div>
        <textarea required='' name='description' id='description'>" . $this->list->description . "</textarea>
        <label for='description'>Description de la liste *</label>
      </div>

      <div>
        <input type='date' name='date' required='' id='date' value='". $this->list->expiration. "'>
        <label for='date'>Date de fin *</label>
      </div>
      
      <div class='input-radio'>
        <p>Voulez vous mettre la liste en publique ?</p>
        
        <label for='yes'>Oui</label>
        <input id='yes' type='radio' name='estPublique' value='true'>
         
        <label for='no'>Non</label>
        <input id='no' type='radio' name='estPublique' value='false' checked> 
      </div>


      <input type='submit' value='Modifier la liste'>

    </form>
    
    
  </div>
   </div>
</section>";
        return $html;
    }
}
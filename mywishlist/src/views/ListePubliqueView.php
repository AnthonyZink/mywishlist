<?php

namespace mywishlist\views;

use mywishlist\models\Liste;
use Slim\Slim;


class ListePubliqueView extends GlobalView
{
    //permet de savoir quel trie effectuer
    private $trie ="";

    public function __construct($trie)
    {
        $this->trie = $trie;
    }

    //methode d'affichage
    public function render(){
        $head = parent::head();
        $header = parent::header();

        $liste = $this->liste();

        $html = "
<html lang='fr'>
    <head>   
        $head
        <link rel='stylesheet' href='assets/css/listePublique.css'>
    </head>
    <body>
    
        $header
    
        
        $liste
        
    

    </body>
</html>
        ";

        echo $html;
    }

    //affiche les listes rendues publiques
    private function liste(){
        $res = "<section id='list_publique'>
                    <div class='container'>
                    <h1>Listes publiques</h1>
                    
                    
                    <div class='content'>
                        <form action='' method='post'>
                                <input type='radio' name='trier' value='titre' checked> Trier par titre 
                                <input type='radio' name='trier' value='expiration' checked> Trier par date 
                                
                                <input type='submit' value='Appliquer'>
                        </form>
                        
                        <div id='lists'>";
        $iterateur = 1;


       $queryList = Liste::where('estPublique', '=', 'true')->where('expiration','>',date('Y-m-d'))->orderBy($this->trie)->get();
        $app = Slim::getInstance();
        foreach ($queryList as $list){
            $create_date = date_create($list->expiration);
            $date = date_format($create_date,'d/m/Y');
            $res .= "
                
                    <div class='list grid-12'>
                        <div class='col-1 num'>
                            <p class='numero-item'>#".$iterateur++."</p>
                        </div>
                        <div class='col-6 nom'>
                            <p>" . $list->titre . "</p>
                        </div>
                        
                        <div class='col-2 expiration'>
                            <p>" . $list->users->username. "</p>
                        </div>
                        
                        <div class='col-2 expiration'>
                            <p>" . $date. "</p>
                        </div>
                        <div class='col-1 loupe'><a href='" . $app->urlFor('liste')  . "?token=" . $list->token . "'>
                        <i id='loupe_1' class='fa fa-search fa-3x'></i></a></div>
                    </div>
                ";
        }

        $res .= "</div>
               </div>
               </div>
            </section>";


        return $res;


}

}
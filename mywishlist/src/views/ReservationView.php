<?php

namespace mywishlist\views;


use mywishlist\models\Item;

class ReservationView extends GlobalView
{

    private $token;
    private $id;

    public function __construct($token,$id)
    {
        $this->id = $id;
        $this->token = $token;

    }


    public function render(){
        $head = parent::head();
        $header = parent::header();

        $reservation = $this->reservation();


        $html = "
<html lang='fr'>
    <head>   
        $head
        <link rel='stylesheet' href='assets/css/reservation.css'>
    </head>
    <body>
    
        $header
        
        $reservation

    </body>
</html>
        ";

        echo $html;
    }



    private function reservation(){

        $item = Item::where("id","=",$_GET['id'])->first();

        $nom = "";

        if(isset($_SESSION['userReserv'])){
            $nom = $_SESSION['userReserv'];
        }

        return "
            <section id='reservation'>
            
              <div class='box'>
                
                    ".parent::error()."
                
                <h2>Reservation <i class='fa fa-gift' aria-hidden='true'></i></h2>
                <em>$item->nom</em>
                <form action='' method='post'>
                      <br>".$this->affichagePrix($item)."
                  <div class='identite''>
                    <div>
                      <input type='text' autocomplete='off' name='nom' required id='nom' value='$nom'>
                      <label for='nom'>Nom et prenom*</label>
                    </div>
                    
                    ".$this->cagnotte($item)."
                    
                    <div id='msg'>
                        <textarea name='textRes' id='textRes' cols='30' rows='10'></textarea>  
                        <label for='textRes'>Votre petit message *</label>
                    </div>
                    
                  </div>
            
                  <input type='hidden' name='id' value='".$_GET['id']."'>
                  <input type='submit' value='Envoyer'>
            
                </form>
              </div>
                
            </section>
        ";
    }

    private function affichagePrix($item){
        if($item->cagnotte == 1){
            return "<p>Il reste $item->tarif € à payer</p>";
        }

        return "<p>$item->tarif €</p>";
    }

    private function cagnotte($item){
        if($item->cagnotte == 1){
            return "<div>
                      <input type='text' autocomplete='off' name='cagnotte' required id='cagnotte' >
                      <label for='cagnotte'>Votre montant </label>
                    </div>";
        }else{
            return "";
        }
    }

}
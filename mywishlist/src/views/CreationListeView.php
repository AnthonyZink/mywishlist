<?php

namespace mywishlist\views;


class CreationListeView extends GlobalView
{
    //methode d'affichage
    public function render(){
        $head = parent::head();
        $header = parent::header();

        $creationListe = $this->creationListe();

        $html = "
<html lang='fr'>
    <head>
        $head
        
        <link rel='stylesheet' href='assets/css/creationListe.css'>
    </head>
    <body>
    
        $header
    
    
        $creationListe
        
    
    
    </body>
</html>";

        echo $html;
    }

    //creation d'une liste
    private function creationListe(){
        $html = "

<section id='creationListe'>
      <div class='container'>
            <div class='box'>
            ".parent::error()."
    <h2>Nouvelle liste <i class='fa fa-gift' aria-hidden='true'></i></h2>
    <form action='' method='post'>
      <div>
        <input type='text' name='username' required='' id='username'>
        <label for='username'>Nom de la liste *</label>
      </div>

      <div>
        <textarea required='' name='description' id='description'></textarea>
        <label for='description'>Description de la liste *</label>
      </div>

      <div>
        <input type='date' name='date' required='' id='date'>
        <label for='date'>Date de fin *</label>
      </div>
      
      <div class='input-radio'>
        <p>Voulez vous mettre la liste en publique ?</p>
        
        <label for='yes'>Oui</label>
        <input id='yes' type='radio' name='estPublique' value='true'>
         
        <label for='no'>Non</label>
        <input id='no' type='radio' name='estPublique' value='false' checked> 
      </div>


      <input type='submit' value='Créer liste'>

    </form>
    
    
  </div>
   </div>
</section>";
    return $html;
    }
}
<?php

namespace mywishlist\views;

class InscriptionView extends GlobalView
{


    //methode d'affichage
    public function render(){

        $head = parent::head();
        $header = parent::header();

        $inscription = $this->inscription();


        $html ="
        
<html lang='fr'>
    <head>
    
        $head
         <link rel='stylesheet' href='assets/css/inscription.css'>
    </head>
    <body>
        
        $header
    
    
        $inscription
        
    
    </body>
</html>
        ";

        echo $html;
    }

    //formulaire d'inscription
    private function inscription(){
        $s = "S'inscrire'";


        return "

<section id='inscription'>

  <div class='box'>
    
        ".parent::error()."
    
    <h2>Inscription <i class='fa fa-gift' aria-hidden='true'></i></h2>
    <form action='' method='post'>
      <div>
        <input type='text' autocomplete='off' name='username' required id='username' autofocus='autofocus'>
        <label for='username'>Nom d'utilisateur *</label>
      </div>
      <div class='identite''>
        <div>
          <input type='text' autocomplete='off' name='nom' required id='nom'>
          <label for='nom'>Nom *</label>
        </div>

        <div>
          <input type='text' autocomplete='off' name='prenom' required  id='prenom'>
          <label for='prenom'>Prénom *</label>
        </div>
      </div>

      <div>
        <input type='text' autocomplete='off' name='email' required id='email'>
        <label for='email'>Adresse email *</label>
      </div>

      <div>
        <input type='password' autocomplete='off' name='password' required id='password'>
        <label for='password'>Mot de passe *</label>
      </div>

      <div>
        <input type='password' autocomplete='off' required name='password_confirm' id='password_confirm'>
        <label for='password_confirm'>Confirmation de mot de passe *</label>
      </div>

      <input type='submit' value='Inscription'>

    </form>
  </div>



    <!-- <div class='inscription-content '>
        <div class='title'>
            <p>Inscription</p>
        </div>
        <div class='formulaire'>
            <form action='' method='post'>
            
                
                    <div class='form-group'>
                        <label for='username'><i class='fa fa-user' aria-hidden='true'></i>Nom d'utilisateur</label><br>
                        <input type='text' id='username' name='username' placeholder='Nom utilisateur' required>
                    </div>
    
                    <div class='form-group'>
                        <label for='email'><i class='fa fa-envelope' aria-hidden='true'></i>Adresse Email</label><br>
                        <input type='email' id='email' name='email' placeholder='Adresse mail' required>
                    </div>
                
                
               
                    <div class='form-group'>
                        <label for='password'><i class='fa fa-unlock' aria-hidden='true'></i>Mot de passe</label><br>
                        <input type='password' id='password' name='password' placeholder='Mot de passe' required>
                    </div>
    
                    <div class='form-group'>
                        <label for='password_confirm'><i class='fa fa-unlock' aria-hidden='true'></i>Confirmation</label><br>
                        <input type='password' id='password_confirm' name='password_confirm' placeholder='Confirmation du mot de passe' required>
                    </div>
                
                
                <div class='form-group'>
                    <input type='submit' name='btn' value='.$s.''>
                </div>
            </form>
        </div>

    </div>-->

</section>";



    }

}
<?php

namespace mywishlist\views;

use mywishlist\models\Liste;


class PartagerListeView extends GlobalView {
    public function render(){
        $head = parent::head();
        $header = parent::header();

        $creationListe = $this->creationListe();

        $html = "
<html lang='fr'>
    <head>
        $head
        
        <link rel='stylesheet' href='assets/css/creationListe.css'>
    </head>
    <body>
    
        $header
    
    
        $creationListe
    
    </body>
</html>";

        echo $html;
    }


    private function creationListe(){
        $queryList = Liste::where('token', '=', $_GET['token'])->first();
        $user = $queryList->users;
        $html = "

        <section id='creationListe'>
          <div class='container'>
                <div class='box'>
                ".parent::error()."
        <h2>Partager votre liste <i class='fa fa-envelope' aria-hidden='true'></i></h2>
        <form action='' method='post'>
          <div>
            <input type='text' name='email' required='' id='email'>
            <label for='email'>Adresse mail de votre destinataire *</label>
          </div>
    
            <div>
            <input type='text' name='objet' required='' id='objet'>
            <label for='objet'>Objet de votre mail *</label>
          </div>
    
          <div>
            <textarea required='' name='texte' id='texte'></textarea>
            <label for='texte'>Contenu de votre message *</label>
          </div>
    
    
          <input type='submit' value='Créer le message' class='submit'>
            
        </form>
        <div class='message'>
        <br>
        ";

        if (isset($_POST) AND !empty($_POST)) {
            $html .= "<i>Voici votre message généré :</i> <br>";
            $html .= "<i>Titre :</i> " . $_POST['objet'] . "<br>";
            $html .= "<i>Adresse du destinataire :</i> " . $_POST['email'] . "<br>";
            $html .= "<i>Adresse de l'envoyeur :</i> " . $user->email . "<br>";
            $html .= "<i>Contenu du message :</i><br> " . $_POST['texte'] . "<br>";
        }


        $html .= "</div></div>
   </div>
</section>";
        return $html;
    }



}
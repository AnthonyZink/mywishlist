<?php

namespace mywishlist\views;


class ContactView extends GlobalView
{


    //methode d'affichage
    public function render(){

        $head = parent::head();
        $header = parent::header();

        $contact = $this->contact();

        $html = "
<html lang='fr'>
    <head>   
        $head
    </head>
    <body>
        
        $header
        <link rel='stylesheet' href='assets/css/contact.css'>
        
        $contact
    
    
    </body>
</html>";

        echo $html;

    }

    
    //formulaire de contact
    private function contact(){
        return "
            <section id='contact'>
                 <div class='box'>
    
                <h2>Nous contacter <i class='fa fa-envelope' aria-hidden='true'></i></h2>
                <form action='' method='post'>
                  <div class='identite''>
                    <div>
                      <input type='text' autocomplete='off' name='nom' required id='nom'>
                      <label for='nom'>Nom *</label>
                    </div>
            
                    <div>
                      <input type='text' autocomplete='off' name='prenom' required  id='prenom'>
                      <label for='prenom'>Prénom *</label>
                    </div>
                  </div>
            
                  <div>
                    <input type='text' autocomplete='off' name='email' required id='email'>
                    <label for='email'>Adresse email *</label>
                  </div>
                  
                  <div>
                    <textarea name='' id='content' cols='30' rows='10'></textarea>
                    <label for='content'>Votre message *</label>
                  </div>
            
                  <input type='submit' value='Envoyer'>
            
                </form>
              </div>

            </section>
        ";
    }


}
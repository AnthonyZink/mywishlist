<?php

namespace mywishlist\views;

use mywishlist\models\Commentaire;
use mywishlist\models\Item;
use Slim\Slim;

class ItemView extends GlobalView
{
    //methode d'affichage
    public function render(){
        $head = parent::head();
        $header = parent::header();

        $item = $this->item();

        $html = "
<html lang='fr'>
    <head>   
        $head
        <link rel='stylesheet' href='assets/css/item.css'>
        <script src=\"assets/jquery-3.2.1.min.js\"></script>
        <script src=\"assets/responsiveslides.min.js\"></script>
        <script>
        
        $(function(){
            
        $('#slider').responsiveSlides({
                 /* auto: true,             // Boolean: Animate automatically, true or false
                  speed: 500,            // Integer: Speed of the transition, in milliseconds
                  timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
                  pager: false,           // Boolean: Show pager, true or false
                  nav: true,             // Boolean: Show navigation, true or false
                  random: false,          // Boolean: Randomize the order of the slides, true or false
                  pause: true,           // Boolean: Pause on hover, true or false
                  pauseControls: true,    // Boolean: Pause when hovering controls, true or false
                  prevText: \"Précédent\",   // String: Text for the \"previous\" button
                  nextText: \"Suivant\",       // String: Text for the \"next\" button
                  maxwidth: \"\",           // Integer: Max-width of the slideshow, in pixels
                  navContainer: \"\",       // Selector: Where controls should be appended to, default is after the 'ul'
                  manualControls: \"\",     // Selector: Declare custom pager navigation
                  namespace: \"rslides\",   // String: Change the default namespace used
                  before: function(){},   // Function: Before callback
                  after: function(){}     // Function: After callback*/
                 auto:true,
                 pause:true,
                 nav:true, 
                 prevText: '<',
                 nextText: '>',   
                 pager:true,
                 speed:2000,
                 namespace:\"rslides\"
             });
        });
            
        
</script>
    </head>
    <body>
    
        $header
    
        $item    
    
        
        
    </body>
</html>
        ";

        echo $html;
    }

    //Affiche en detail d'un item
    private function item(){

    $app = Slim::getInstance();

    if(isset($_GET['id'],$_GET['token'])){
        $s = filter_var($_GET['id'], FILTER_VALIDATE_INT);
        $token = filter_var($_GET['token'],FILTER_SANITIZE_STRING);

        if($s){
            $queryItem = Item::where('id', '=', $s)->first();

            if(isset($queryItem) && $queryItem->liste->token === $token){
                $tarif = $queryItem->tarif;

                $desc = $queryItem->descr;

                $nom = $queryItem->nom;

                $images = $queryItem->images;



                $str =
                    "<section id='item'>

                    <div class='container'>
                            <a id='liste' href='liste?token=" . $token . "'>Retourner à la liste</a>
                        
                        
                        
                            <div id='itemImg' class='col-1 has-gutter'>
                                <ul class=\"rslides\"  id='slider'>".

                    $this->getImages($images)

                    ."
                                </ul>
                            </div>
                            
                            
                            
                            <div class='itemDesc'>
                                <p id='nomItem' class='col-1 has-gutter'> Nom :" . $nom . "</p>
                                <p id='tarifItem' class='col-1 has-gutter'>" . $tarif . "€</p>
                                <p> Description :" . $desc . "</p>
                                ".$this->isReserved($queryItem)."
                                ".$this->url($queryItem)."
                            </div>
					
                </div>
				
		
				
			</section>";

            }
            else{
                $app->redirect('home');
            }
        }
        else{
            $app->redirect('home');
        }
    }else{
        $app->redirect('home');
    }


	
        
        return $str;
    }

    private function url($item){
        if($item->url){
            return "<a href='$item->url' target='_blank'>Lien ver l'objet</a>";
        }
    }

    private function isReserved($item){
        $tab = [];
        if($item->reserve === 1){
            $com = Commentaire::where('id_item',$item->id)->first();
            $tab["nom"]= "L'article est réservé par ".$com->nomUser;
            if($item->liste->expiration < date('Y-m-d')){
                $tab["message"] = $com->content;
            }
        }

        $res = "";
        if(isset($tab['nom'])){
            $res  ="<p>Personne ayant réservé : ".$tab['nom']."</p><br>";
        }

        if(isset($tab['message'])){
            $res  .="<p>Son message : ".$tab['message']."</p>";
        }

        return $res;
    }

    private function getImages($img){
        $s = '';

         foreach($img as $im){
             if($im->url === 0){

                 $s .= "\n <li><img src='assets/img/" .  $im->img . "' alt=''></li>";
             }else{
                 $s .= "\n <li><img src='" .  $im->img . "' alt=''></li>";
             }
         }
        if(empty($s))
            $s .= "\n <li><img src='assets/img/null.jpg' alt=''></li>";


        return $s;
    }

}
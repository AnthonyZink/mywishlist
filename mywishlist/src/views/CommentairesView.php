<?php


namespace mywishlist\views;


use mywishlist\models\Commentaire;
use mywishlist\models\Item;

class CommentairesView extends GlobalView
{


    public function render(){
        $head = parent::head();
        $header = parent::header();


        $commentaires = $this->comms();

        $html = "
            <html lang='fr'>
                <head>   
                    $head
                    <link rel='stylesheet' href='assets/css/commentaire.css'>
                </head>
                <body>
                
                    $header
                    
                    $commentaires
                 
                </body>
            </html>
        ";

        echo $html;
    }

    private function comms(){

        $item = Item::where('id','=',$_GET['id'])->first();
        $comms = Commentaire::where('id_item','=',$_GET['id'])->orderBy('created_at','DESC')->get();



        $res_comms ="";



            foreach ($comms as $comm) {

                $res_comms .= "
                <div class='commentaire'>
                    <div class='commentaire_title'>
                         <p><strong>De : </strong>$comm->nomUser</p>
                    </div>
                    
                    <div class='commentaire_content'>
                        <p>$comm->content</p>
                    </div>
                    
                    <div class='commentaire_footer'>
                        <p>" . date_format(date_create($comm->created_at), 'd/m/Y') . "</p>
                    </div>
                </div>
                
                
            
            ";

            }


        return
    "
        <section id='commentaire'>
            
            <h1>Commentaires de l'item :</h1>
            <p>$item->nom</p>
                $res_comms
                
        
        
        </section>   
    
    ";
    }

}
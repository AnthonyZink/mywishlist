<?php
namespace mywishlist\models;

class Image extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'image';
    protected $primaryKey = ['id_item', 'img'];
    public $incrementing = false;
    protected $keyType = 'string';//?
    public $timestamps = false;

    //un item appartient a une image
    public function item(){
        return $this->belongsTo('mywishlist\models\Item','id');
    }


    protected function setKeysForSaveQuery(\Illuminate\Database\Eloquent\Builder $query) {
        if (is_array($this->primaryKey)) {
            foreach ($this->primaryKey as $pk) {
                $query->where($pk, '=', $this->original[$pk]);
            }
            return $query;
        }else{
            return parent::setKeysForSaveQuery($query);
        }
    }


}

<?php
namespace mywishlist\models;

class Liste extends \Illuminate\Database\Eloquent\Model{

  protected $table='liste';
  protected $primaryKey='no';
    protected $fillable = ['estPublique'];
  public $timestamps=false;

  //une liste a plusieurs items
  public function items(){
    return $this->hasMany('mywishlist\models\Item','liste_id');
  }

  //une liste appartient a un utilisateur
  public function users(){
      return $this->belongsTo('mywishlist\models\User','user_id');
  }

}

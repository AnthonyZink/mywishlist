<?php

namespace mywishlist\models;


class User extends \Illuminate\Database\Eloquent\Model
{

    protected $table='user';
    protected $primaryKey='uid';
    public $timestamps=false;

    //un utilisateur a un role
    public function roles()
    {
        return $this->belongsToMany('mywishlist\models\Role');
    }

    //un utilisateur a plusieurs listes
    public function lists(){
        return $this->hasMany('mywishlist\models\Liste','user_id');
    }

}
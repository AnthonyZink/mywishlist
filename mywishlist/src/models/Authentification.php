<?php
/**
 * Created by PhpStorm.
 * User: antho
 * Date: 13/12/2017
 * Time: 18:56
 */

namespace mywishlist\models;


use Slim\Slim;

class Authentification
{


    //creation d'un utilisateur
    public static function createUser($username, $password, $email, $nom, $prenom){

        $verif = User::where('username','=',$username)->first();
        $app = Slim::getInstance();

        if(!$verif){
            //insertion de l'utilisateur
            $user = new User();
            $user->username = $username;
            $user->password = $password;
            $user->email = $email;
            $user->nom = $nom;
            $user->prenom = $prenom;
            $user->save();


            //insertion du role de l'utilisateur dans la table role_user
            $u = User::where('username','=',$username)->first();


            $role_user = new Role_user();
            $role_user->user_uid = $u->uid;
            $role_user->role_roleid = 1;
            $role_user->save();
        }else{
            $app->flash('error', "L'utilisateur existe déjà");
            $app->redirect('connexion');
        }




    }


    //authentification d'un utilisateur
    public static function authentificate($username, $password){
        $app = Slim::getInstance();

        //on recupere l'utilisateur dans la base de donnee
        $user = User::where('username','=',$username)->first();

        //s'il existe
        if($user){

            //on verifie que le mot de passe entree correspond a celui da la base de donnee
            if(password_verify($password,$user->password)){

                //on le connecte
                self::loadProfil($user->uid);

            }else{
                $app->flash('error', 'Mot de passe ou utilisateur incorrect');
                $app->redirect('connexion');
            }
        }else{
            $app->flash('error', 'Mot de passe ou utilisateur incorrect');
            $app->redirect('connexion');
        }


    }


    //connexion d'un utilisateur
    public static function loadProfil($uid){

        //on recupere l'utilisateur grace a son id dans la base de donnee
        $user = User::where('uid','=',$uid)->first();

        //s'il existe une session en cour on la detruit
        if(isset($_SESSION['profile'])){
            unset($_SESSION['profile']);
        }

        $r = 0;
        $auth_lvl=0;

        //on recupere le role et le niveau d'authentification de l'utilisateur
        foreach ($user->roles as $role){
            $r= $role->roleid;
            $auth_lvl=$role->auth_level;
        }


        //creation de la nouvelle session
        $_SESSION['profile'] = [
            "username"   => $user->username,
            "userid"     => $user->uid,
            "role_id"    => $r,
            'client_ip'  =>$_SERVER['REMOTE_ADDR'],
            "auth_level" => $auth_lvl
        ];

    }


    //verification des droits d'acces d'un utilisateur
    public static function checkAcessRight($required){
        $app = Slim::getInstance();

        if(isset($_SESSION['profile'])){

            //on verifie si l'utilisateur connecte a le niveau requis
            if($_SESSION['profile']['auth_level'] < $required){
                $app->flash('error', "Pas le droit d'aceeder a cette page");
                $app->redirect('connexion');
            }
        }
    }

}
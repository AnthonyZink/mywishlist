<?php

namespace mywishlist\models;


class Role extends \Illuminate\Database\Eloquent\Model
{
    protected $table='role';
    protected $primaryKey='roleid';
    public $timestamps=false;

    //un role appartient a un utilisateur
    public function users()
    {
        return $this->belongsToMany('mywishlist\models\User');
    }
}
<?php

namespace mywishlist\models;


use Illuminate\Database\Eloquent\Model;

class Commentaire extends Model
{

    protected $table = 'commentaire';
    protected $primaryKey ='idCom';
    public $timestamps = true;

    public function item(){
        return $this->belongsTo("mywishlist\models\Commentaire");
    }


}
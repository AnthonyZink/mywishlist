<?php
namespace mywishlist\models;

class Item extends \Illuminate\Database\Eloquent\Model{

  protected $table = 'item';
  protected $primaryKey ='id';
  public $timestamps = false;

  //un item appartient a une liste
  public function liste(){
    return $this->belongsTo('mywishlist\models\Liste','liste_id');
  }

  public function images(){
      return $this->hasMany('mywishlist\models\Image', 'id_item');
  }

    public function commentaire(){
        return $this->belongsTo('mywishlist\models\Commentaire');
    }
}

<?php

namespace mywishlist\models;


class Role_user extends \Illuminate\Database\Eloquent\Model{

    protected $table='role_user';
    protected $primaryKey='user_uid';
    public $timestamps=false;
}